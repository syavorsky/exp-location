angular.module('App', [])
  
  .config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode({
      enabled      : true,
      requireBase  : false,
      rewriteLinks : false
    })
  }])

  .controller('Page1', ['$scope', '$location', '$interval', function($scope, $location, $interval) {
    $scope.pages = [1,2,3,4,5];

    $scope.page = $scope.pages[0];

    $scope.gotoPage = function(page) {
      $scope.page = page;
      $location.search({pageNumber: $scope.page});
    }

    $scope.count = 0;
    $interval(function() {
      $scope.count++;
    }, 1000);
  }]);
